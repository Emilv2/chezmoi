#!/usr/bin/zsh
# Line
export PATH=~/.local/bin:~/bin:$PATH
# needed to make Xilinx ISE work properly
xsource /opt/Xilinx/14.7/ISE_DS/settings64.sh
setopt appendhistory extendedglob PROMPT_SUBST
# End of lines configured by zsh-newuser-install
export HISTSIZE=5000000
export TERM=xterm-256color
#vi mode
bindkey -v

# fzf, after bindkey -v
xsource /usr/share/fzf/key-bindings.zsh
xsource /usr/share/fzf/completion.zsh

export LC_ALL=en_US.UTF-8

# cd direct into a directory in parent directory or current
# don't do this for now
# cdpath=(.. ~)
# but first autocomplete current directory
zstyle ':completion:*:complete:(cd|pushd):*' tag-order 'local-directories named-directories'


#less delay for switch mode
export KEYTIMEOUT=1

# some aliases
alias ls='ls --color=auto'
alias cd..='cd ..'
alias untar='tar -xvzf'
alias syn='rsync -vrhP'
alias vi='nvim'
# trick to make aliases run with sudo
# https://linuxhandbook.com/run-alias-as-sudo/
alias sudo='sudo '
alias ll='ls -lh'
alias la='ls -a'
alias emc='emacsclient -t'

alias venv='. venv'

# don't follow ignore files when we are in $HOME
alias rg='ripgrep_ignore_fix'

# Xilinx FPGA ISE & impact
alias ise='/opt/Xilinx/14.7/ISE_DS/ISE/bin/lin64/ise'
alias impact='/opt/Xilinx/14.7/ISE_DS/ISE/bin/lin64/impact'

#use vim instead of vi as default editor
export EDITOR=/usr/bin/nvim
export VISUAL=/usr/bin/nvim
export BROWSER="firefox"


export DISABLE_AUTO_TITLE="true"

####### better vi bindings #######
# https://old.reddit.com/r/vim/comments/7wj81e/you_can_get_vim_bindings_in_zsh_and_bash/du3tx3z/
## ci"
autoload -U select-quoted
zle -N select-quoted
for m in visual viopp; do
  for c in {a,i}{\',\",\`}; do
    bindkey -M $m $c select-quoted
  done
done

# ci{, ci(, ci[, ci<
autoload -U select-bracketed
zle -N select-bracketed
for m in visual viopp; do
  for c in {a,i}${(s..)^:-'()[]{}<>bB'}; do
    bindkey -M $m $c select-bracketed
  done
done

# vim surround
# like https://github.com/tpope/vim-surround
autoload -Uz surround
zle -N delete-surround surround
zle -N add-surround surround
zle -N change-surround surround
bindkey -a cs change-surround
bindkey -a ds delete-surround
bindkey -a ys add-surround
bindkey -M visual S add-surround

# https://github.com/lf94/peek-for-tmux
function peek { tmux split-window -p 33 ${EDITOR:-vi} "$@"}

# run command if nomatch if found
# this way HEAD^ works in git
unsetopt nomatch

DARK_THEME=1
function toggle_theme() {
  if [[ "$DARK_THEME" == "1" ]]; then
    DARK_THEME=0
    if [ $TMUX ]; then
      tmux set -g status-bg color187
      tmux set -g status-fg color66
    fi
      if [[ "$YAKUAKE" == "1" ]]; then
        # https://askubuntu.com/questions/416572/switch-profile-in-konsole-from-command-line
        printf '\033Ptmux;\033\033]50;konsoleprofile colors=SolarizedLight\007\033\\'
      elif [[ "$ALACRITTY" == "1" ]]; then
        sed -i 's/^colors: \*dark$/colors: *light/g' ~/.config/alacritty/alacritty.yml
      fi
  else
    DARK_THEME=1
    if [ $TMUX ]; then
      tmux set -g status-bg black
      tmux set -g status-fg white
    fi
    if [[ "$YAKUAKE" == "1" ]]; then
      printf '\033Ptmux;\033\033]50;konsoleprofile colors=Solarized\007\033\\'
    elif [[ "$ALACRITTY" == "1" ]]; then
      sed -i 's/^colors: \*light$/colors: *dark/g' ~/.config/alacritty/alacritty.yml
    fi
  fi
}

function escape_regex_quote() {
    sed 's/[][\.|$(){}?+*^]/\\&/g' <<< "$*"
}

# https://unix.stackexchange.com/a/291611
# CC BY-SA 3.0
function path_remove {
  # Delete path by parts so we can never accidentally remove sub paths
  PATH=${PATH//":$1:"/":"} # delete any instances in the middle
  PATH=${PATH/#"$1:"/} # delete any instance at the beginning
  PATH=${PATH/%":$1"/} # delete any instance in the at the end
}

# autocomplete the venv script
compdef _venv venv
function _venv {
  local -a subcmds
  subcmds=('remove:(deactivate) and remove virtualenv' 'recreate:recreate and activate virtualenv')
  _describe 'command' subcmds #-- topics
}

# kdesrc-build #################################################################

## Add kdesrc-build to PATH
export PATH="$HOME/code/kde/src/kdesrc-build:$PATH"

autoload bashcompinit
bashcompinit

## Autocomplete for kdesrc-run
function _comp-kdesrc-run
{
  local cur
  COMPREPLY=()
  cur="${COMP_WORDS[COMP_CWORD]}"

  # Complete only the first argument
  if [[ $COMP_CWORD != 1 ]]; then
    return 0
  fi

  # Retrieve build modules through kdesrc-run
  # If the exit status indicates failure, set the wordlist empty to avoid
  # unrelated messages.
  local modules
  if ! modules=$(kdesrc-run --list-installed);
  then
      modules=""
  fi

  # Return completions that match the current word
  COMPREPLY=( $(compgen -W "${modules}" -- "$cur") )

  return 0
}

## Register autocomplete function
complete -o nospace -F _comp-kdesrc-run kdesrc-run

################################################################################

